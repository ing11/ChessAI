#pragma once

#include "chessboard.hh"
#include "network-api/client-network-api.hh"

class Client
{
  public:
    Client(const std::string& ip, const std::string& port,
        const std::string& login);
    void init();
    void play();
  protected:
    bool get_other_move(std::string& res);
    bool get_other_move_comm(std::string& res);
    bool exec_move(std::string, bool color);
    network_api::ClientNetworkAPI server_;
    plugin::ChessBoard board_;
    bool color_black_;
};
