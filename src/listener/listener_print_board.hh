#include <listener.hh>

namespace plugin
{
  class PrinterBoard : public Listener
  {
    public:
      void register_board(const ChessboardInterface& board_interface) override;
      void on_game_started() override;
      void on_game_finished() override;
      void on_piece_moved(const PieceType piece,
          const Position& from,
          const Position& to) override;
      void on_piece_taken(const PieceType piece,
          const Position& at) override;
      void on_piece_promoted(const PieceType piece,
          const Position& at) override;
      void on_kingside_castling(const Color color) override;
      void on_queenside_castling(const Color color) override;
      void on_player_check(const Color color) override;
      void on_player_mat(const Color color) override;
      void on_player_pat(const Color color) override;
      void on_player_timeout(const Color color) override;
      void on_player_disqualified(const Color color) override;
      void on_draw() override;
  };
}
