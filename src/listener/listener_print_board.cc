#include "listener_print_board.hh"

namespace plugin
{
  static std::string pos_to_string(const Position& pos);
  static std::string piece_to_string(const PieceType& piece);

  LISTENER_EXPORT(PrinterBoard);
  void PrinterBoard::register_board(const ChessboardInterface& board_interface)
  {
    std::cout << "board registered" << std::endl;
  }
  void PrinterBoard::on_game_started()
  {
    std::cout << "game started" << std::endl;
  }
  void PrinterBoard::on_game_finished()
  {
    std::cout << "game finished" << std::endl;
  }
  void PrinterBoard::on_piece_moved(const PieceType piece,
      const Position& from,
      const Position& to)
  {
    std::cout << "move [" << piece_to_string(piece) << "] start: "
      << pos_to_string(from) << " end: " << pos_to_string(to) << std::endl;
  }
  void PrinterBoard::on_piece_taken(const PieceType piece, const Position& at)
  {
    std::cout << "piece taken: " << piece_to_string(piece) << " at "
      << pos_to_string(at) << std::endl;
  }
  void PrinterBoard::on_piece_promoted(const PieceType piece, const Position& at)
  {
    std::cout << "piece promoted: " << piece_to_string(piece) << " at "
      << pos_to_string(at) << std::endl;
  }
  void PrinterBoard::on_kingside_castling(const Color color)
  {
    std::cout << "player " << (((bool)color) ? "black" : "white")
      << " do a king castling" << std::endl;
  }
  void PrinterBoard::on_queenside_castling(const Color color)
  {
    std::cout << "player " << (((bool)color) ? "black" : "white")
      << " do a queen castling" << std::endl;
  }
  void PrinterBoard::on_player_check(const Color color)
  {
    std::cout << "player " << (((bool)color) ? "black" : "white") << " is check"
      << std::endl;
  }
  void PrinterBoard::on_player_mat(const Color color)
  {
    std::cout << "player " << (((bool)color) ? "black" : "white") << " is mat"
      << std::endl;
  }
  void PrinterBoard::on_player_pat(const Color color)
  {
    std::cout << "player " << (((bool)color) ? "black" : "white") << " is pat"
      << std::endl;
  }
  void PrinterBoard::on_player_timeout(const Color color)
  {
    std::cout << "player " << (((bool)color) ? "black" : "white") << " is timeout"
      << std::endl;
  }
  void PrinterBoard::on_player_disqualified(const Color color)
  {
    std::cout << "player " << (((bool)color) ? "black" : "white") << " is disqualified"
      << std::endl;
  }
  void PrinterBoard::on_draw()
  {
    std::cout << "game draw" << std::endl;
  }
  static std::string pos_to_string(const plugin::Position& pos)
  {
    std::string res;
    res = ((int)(pos.file_get()) + 'a');
    res += ((int)(pos.rank_get()) + '1');
    return res;
  }

  static std::string piece_to_string(const PieceType& piece)
  {
    switch (piece)
    {
      case PieceType::KING:
        return "King";
      case PieceType::QUEEN:
        return "Queen";
      case PieceType::ROOK:
        return "Rook";
      case PieceType::BISHOP:
        return "Bishop";
      case PieceType::KNIGHT:
        return "Knight";
      case PieceType::PAWN:
        return "Pawn";
    }
  }
}
