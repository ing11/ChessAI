#include <iostream>
#include <dlfcn.h>
#include <map>
#include <utility>

#include "server.hh"
#include "tool_listener.hh"

static std::pair<plugin::Position, plugin::Position> convert(std::string& str);

Server::Server(unsigned short port, std::vector<std::string>& libs)
  : player1_(network_api::ServerNetworkAPI(port)),
  player2_(network_api::ServerNetworkAPI(port)),
  board_(plugin::ChessBoard())
{
  player1_.acknowledge(false);
  player2_.acknowledge(true);
  for (auto i : libs)
  {
    void* lib = dlopen(i.c_str(), RTLD_LAZY);
    libraries_.push_back(lib);
    void* symbol = dlsym(lib, "listener_create");
    listeners_.push_back(reinterpret_cast<plugin::Listener*(*)()>(symbol)());
  }
}

Server::~Server()
{
  if (debug_)
    board_.print();
  listener_on_game_finished(listeners_);
  for (auto i : libraries_)
    dlclose(i);
}

bool Server::start()
{
  player1_.send("uci");
  std::string response1 = player1_.receive();
  if (response1.compare("uciok"))
  {
    std::cerr << "Invalid response protocol from white player.";
    return false;
  }
  player2_.send("uci");
  std::string response2 = player2_.receive();
  if (response2.compare("uciok"))
  {
    std::cerr << "Invalid response protocol from black player.";
    return false;
  }
  player1_.send("isready");
  response1 = player1_.receive();
  if (response1.compare("readyok"))
  {
    std::cerr << "Invalid response protocol from white player.";
    return false;
  }
  player2_.send("isready");
  response2 = player2_.receive();
  if (response2.compare("readyok"))
  {
    std::cerr << "Invalid response protocol from black player";
    return false;
  }
  listener_register_board(listeners_, board_);
  return true;
}

void Server::play()
{
  listener_on_game_started(listeners_);
  if (debug_)
    board_.print();  ///////////
  player1_.send("ucinewgame");
  player1_.send("go");
  std::string response;
  if (!get_move(plugin::Color::WHITE, response))
    return ;
  std::string move = response.substr(9);
  if (!check_move(plugin::Color::WHITE, move))
    return ;
  if (!check_state(plugin::Color::BLACK))
    return ;
  msg_ += " " + move;
  if (debug_)
    board_.print();   ///////////
  player2_.send("ucinewgame");
  player2_.send(msg_);
  player2_.send("go");
  if (!get_move(plugin::Color::BLACK, response))
    return ;
  move = response.substr(9);
  if (!check_move(plugin::Color::BLACK, move))
    return ;
  if (!check_state(plugin::Color::WHITE))
    return ;
  msg_ += " " + move;
  bool turn = true;
  plugin::Color color = plugin::Color::WHITE;
  while (1)
  {
    if (debug_)
      board_.print(); //////////////
    auto& player = (turn) ? player1_ : player2_;
    player.send(msg_);
    player.send("go");
    if (!get_move(color, response))
      return ;
    move = response.substr(9);
    if (!check_move(color, move))
      return ;
    msg_ += " " + move;
    turn = !turn;
    color = (turn) ? plugin::Color::WHITE : plugin::Color::BLACK;
    if (!check_state(color))
      return ;
  }
}

static std::pair<plugin::Position, plugin::Position> convert(std::string& str)
{
  static std::map<char, plugin::File> map_file = {
    {'a', plugin::File::A },
    {'b', plugin::File::B },
    {'c', plugin::File::C },
    {'d', plugin::File::D },
    {'e', plugin::File::E },
    {'f', plugin::File::F },
    {'g', plugin::File::G },
    {'h', plugin::File::H }};
  static std::map<char, plugin::Rank> map_rank = {
    {'1', plugin::Rank::ONE},
    {'2', plugin::Rank::TWO},
    {'3', plugin::Rank::THREE},
    {'4', plugin::Rank::FOUR},
    {'5', plugin::Rank::FIVE},
    {'6', plugin::Rank::SIX},
    {'7', plugin::Rank::SEVEN},
    {'8', plugin::Rank::EIGHT}};
  plugin::Position start(map_file[str[0]], map_rank[str[1]]);
  plugin::Position end(map_file[str[2]], map_rank[str[3]]);
  return std::make_pair(start, end);
}

bool Server::get_move(plugin::Color color, std::string& res)
{
  auto& client = (color == plugin::Color::WHITE) ? player1_ : player2_;
  try
  {
    res = client.receive();
    return true;
  }
  catch (std::exception)
  {
    listener_on_player_timeout(listeners_, color);
    listener_on_player_disqualified(listeners_, color);
    return false;
  }
}

bool Server::check_move(plugin::Color color, std::string& move)
{
  static double cpt = 1.0;
  auto pair = convert(move);
  auto old_piece = board_[pair.second];
  switch(board_.move(color, pair.first, pair.second))
  {
    case plugin::move_ret::CAST_KING:
      {
        auto piece = board_[pair.second];
        listener_on_piece_moved(listeners_, piece->first, pair.first,
            pair.second);
        listener_on_kingside_castling(listeners_, color);
        break;
      }
    case plugin::move_ret::CAST_QUEEN:
      {
        auto piece = board_[pair.second];
        listener_on_piece_moved(listeners_, piece->first, pair.first,
            pair.second);
        listener_on_queenside_castling(listeners_, color);
        break;
      }
    case plugin::move_ret::SUCC:
      {
        auto piece = board_[pair.second];
        if (piece->first == plugin::PieceType::PAWN) {
          cpt = 1.0;
          saves_.clear();
        }
        listener_on_piece_moved(listeners_, piece->first, pair.first,
            pair.second);
        break;
      }
    case plugin::move_ret::FAIL:
      listener_on_player_disqualified(listeners_, color);
      if (debug_)
        std::cout << "MOVE FAILED= " << move << std::endl;
      return false;
      break;
    case plugin::move_ret::TAKEN:
      {
        saves_.clear();
        cpt = 1.0;
        auto piece = board_[pair.second];
        listener_on_piece_moved(listeners_, piece->first, pair.first,
            pair.second);
        listener_on_piece_taken(listeners_, old_piece->first, pair.second);
        break;
      }
    case plugin::move_ret::PROMOTE:
      {
        board_.promote(pair.second, (plugin::PieceType)(move[4]), color);
        auto piece = board_[pair.second];
        listener_on_piece_moved(listeners_, piece->first, pair.first,
            pair.second);
        listener_on_piece_promoted(listeners_, piece->first, pair.second);
        break;
      }
    case plugin::move_ret::PT:
      {
        saves_.clear();
        cpt = 1.0;
        board_.promote(pair.second, (plugin::PieceType)(move[4]), color);
        auto piece = board_[pair.second];
        listener_on_piece_moved(listeners_, piece->first, pair.first,
            pair.second);
        listener_on_piece_taken(listeners_, old_piece->first, pair.second);
        listener_on_piece_promoted(listeners_, piece->first, pair.second);
        break;
      }
    case plugin::move_ret::PASS:
    {
      saves_.clear();
      cpt = 1.0;
      auto piece = board_[pair.second];
      listener_on_piece_moved(listeners_, piece->first, pair.second,
              pair.second);
      int inc = (color == plugin::Color::WHITE) ? -1 : 1;
      listener_on_piece_taken(listeners_, plugin::PieceType::PAWN,
                              plugin::Position(pair.second.file_get(),
                                               (plugin::Rank)((int)(pair.second.rank_get()) + inc)));
      break;
    }
  }
  cpt += 0.5;
  if (cpt >= 50.0) {
    listener_on_draw(listeners_);
    return false;
  }
  if (insert_save()) {
    listener_on_draw(listeners_);
    return false;
  }
  return true;
}

bool Server::check_state(plugin::Color color)
{
  if (board_.check_check(color))
  {
    if (board_.check_checkmate(color))
    {
      listener_on_player_mat(listeners_, color);
      return false;
    }
    listener_on_player_check(listeners_, color);
    return true;
  }
  if (board_.check_draw(color))
  {
    listener_on_player_pat(listeners_, color);
    listener_on_draw(listeners_);
    return false;
  }
  // check draw : listener_on_player_draw(); return false;
  return true;
}

bool Server::insert_save(){
  auto pair = std::make_pair(board_.get_Wpos(), board_.get_Bpos());
  auto search = saves_.find(pair);
  if (search != saves_.end()) {
    saves_[pair]++;
    if (saves_[pair] >= 3)
      return true;
  } else {
    saves_.emplace(pair, 1);
  }
  return false;
}
