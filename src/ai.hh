#pragma once

#include <boost/asio/io_service.hpp>
#include <boost/bind.hpp>
#include <boost/thread/thread.hpp>
#include <chrono>
#include <mutex>
#include <condition_variable>

#include "client.hh"
#include "network-api/client-network-api.hh"

class AI
{
  public:
    AI(std::string& ip, std::string& port, std::string& login);
    ~AI();
    void init();
    void play();
  private:
    int eval(const plugin::ChessBoard& board) const;
    int alphabeta(const plugin::ChessBoard& board, int alpha, int beta,
        int count, bool turn, char& bonus);
    int promote_move(plugin::ChessBoard& board, plugin::Position& pos,
        int alpha, int beta, int count, bool turn, char& bonus);
    void next_move_thread(std::vector<std::pair<int, std::string>>& vec,
        int index, int best, plugin::Position& start, plugin::Position& end);
    std::string next_move();
    bool get_other_move(std::string& res);
    bool get_other_move_comm(std::string& res);
    bool exec_move(std::string, bool color);
    bool check_draw(const plugin::ChessBoard& board);
    //void freeze(int* compute_thr_end);

    network_api::ClientNetworkAPI server_;
    plugin::ChessBoard board_;
    bool color_black_;
    std::map<std::pair<uint64_t, uint64_t>, uint8_t> saves_;
    boost::asio::io_service io_service_;
    boost::thread_group threadpool_;
    boost::asio::io_service::work work_;
    std::atomic<int> runnings_;
    std::mutex mutex_;
    std::condition_variable cv_;
    //std::mutex locks_[3];
    int tree_size_;
};
