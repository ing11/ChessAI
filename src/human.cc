//
// Created by dauver_a on 14/05/17.
//

#include <boost/program_options.hpp>

#include "parser.hh"
#include "client.hh"

int main(int argc, char** argv) {
  std::string port;
  std::string ip;
  std::string login("ChessLeader");
  auto listeners = std::vector<std::string>();
  try {
    namespace po = boost::program_options;
    po::options_description desc("Options");
    desc.add_options()
            ("help,h", "show usage")
            ("port,p", po::value<std::string>(), "select the listening port for the network")
            ("ip,i", po::value<std::string>(), "ip to connect")
            ("login,l", po::value<std::string>(), "login: default ChessLeader");

    po::variables_map vm;

    try {
      po::store(po::parse_command_line(argc, argv, desc), vm);

      if (vm.count("help")) {
        std::cout << "Commands:" << std::endl << desc << std::endl;
        return 0;
      }

      if (!(vm.count("ip") || vm.count("port"))) {
        std::cerr << "No port or ip given" << std::endl;
        return 0;
      }

      if (vm.count("login")) {
        login = vm["login"].as<std::string>();
      }

      if (vm.count("port")) {
        port = vm["port"].as<std::string>();
      }

      if (vm.count("ip")) {
        Client client(vm["ip"].as<std::string>(), port, login);
        client.init();
        client.play();
        return 0;
      }
      po::notify(vm);
    }
    catch(po::error& e) {
      std::cerr << "ERROR: " << e.what() << std::endl;
      return 1;
    }
  }
  catch(std::exception& e) {
    std::cerr << e.what() << std::endl;
  }
  return 0;
}
