#include "tool_listener.hh"


namespace plugin
{
  void listener_register_board(std::vector<Listener*>& listeners,
      ChessboardInterface& interface)
  {
    for (Listener* l : listeners)
      l->register_board(interface);
  }
  void listener_on_game_started(std::vector<Listener*>& listeners)
  {
    for (Listener* l : listeners)
      l->on_game_started();
  }
  void listener_on_game_finished(std::vector<Listener*>& listeners)
  {
    for (Listener* l : listeners)
      l->on_game_finished();
  }
  void listener_on_piece_moved(std::vector<Listener*>& listeners,
      const PieceType piece, const Position& from, const Position& to)
  {
    for (Listener* l : listeners)
      l->on_piece_moved(piece, from, to);
  }
  void listener_on_piece_taken(std::vector<Listener*>& listeners,
      const PieceType piece, const Position& at)
  {
    for (Listener* l : listeners)
      l->on_piece_taken(piece, at);
  }
  void listener_on_piece_promoted(std::vector<Listener*>& listeners,
      const PieceType piece, const Position& at)
  {
    for (Listener* l : listeners)
      l->on_piece_promoted(piece, at);
  }
  void listener_on_kingside_castling(std::vector<Listener*>& listeners,
      const Color color)
  {
    for (Listener* l : listeners)
      l->on_kingside_castling(color);
  }
  void listener_on_queenside_castling(std::vector<Listener*>& listeners,
      const Color color)
  {
    for (Listener* l : listeners)
      l->on_queenside_castling(color);
  }
  void listener_on_player_check(std::vector<Listener*>& listeners,
      const Color color)
  {
    for (Listener* l : listeners)
      l->on_player_check(color);
  }
  void listener_on_player_mat(std::vector<Listener*>& listeners,
      const Color color)
  {
    for (Listener* l : listeners)
      l->on_player_mat(color);
  }
  void listener_on_player_pat(std::vector<Listener*>& listeners,
      const Color color)
  {
    for (Listener* l : listeners)
      l->on_player_pat(color);
  }
  void listener_on_player_timeout(std::vector<Listener*>& listeners,
      const Color color)
  {
    for (Listener* l : listeners)
      l->on_player_timeout(color);
  }
  void listener_on_player_disqualified(std::vector<Listener*>& listeners,
      const Color color)
  {
    for (Listener* l : listeners)
      l->on_player_disqualified(color);
  }
  void listener_on_draw(std::vector<Listener*>& listeners)
  {
    for (Listener* l : listeners)
      l->on_draw();
  }
}
