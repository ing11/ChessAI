#pragma once

#include "plugin/listener.hh"
#include <vector>
namespace plugin
{
  void listener_register_board(std::vector<Listener*>& listeners,
      ChessboardInterface& interface);
  void listener_on_game_started(std::vector<Listener*>& listeners);
  void listener_on_game_finished(std::vector<Listener*>& listeners);
  void listener_on_piece_moved(std::vector<Listener*>& listeners,
      const PieceType piece, const Position& from, const Position& to);
  void listener_on_piece_taken(std::vector<Listener*>& listeners,
      const PieceType piece, const Position& at);
  void listener_on_piece_promoted(std::vector<Listener*>& listeners,
      const PieceType piece, const Position& at);
  void listener_on_kingside_castling(std::vector<Listener*>& listeners,
      const Color color);
  void listener_on_queenside_castling(std::vector<Listener*>& listeners,
      const Color color);
  void listener_on_player_check(std::vector<Listener*>& listeners,
      const Color color);
  void listener_on_player_mat(std::vector<Listener*>& listeners,
      const Color color);
  void listener_on_player_pat(std::vector<Listener*>& listeners,
      const Color color);
  void listener_on_player_timeout(std::vector<Listener*>& listeners,
      const Color color);
  void listener_on_player_disqualified(std::vector<Listener*>& listeners,
      const Color color);
  void listener_on_draw(std::vector<Listener*>& listeners);
}
