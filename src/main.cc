#include <boost/program_options.hpp>

#include "parser.hh"
#include "server.hh"
#include "tool_listener.hh"

int main(int argc, char** argv) {
  unsigned port;
  auto listeners = std::vector<std::string>();
  try {
    namespace po = boost::program_options;
    po::options_description desc("Options");
    desc.add_options()
      ("help,h", "show usage")
    ("port,p", po::value<unsigned>(), "select the listening port for the network")
      ("pgn", po::value<std::string>(), "path to the PGN game file")
      ("listeners,l", po::value<std::vector<std::string> >()->multitoken(),
       "list of paths to listeners plugins")
      ("debug,d", po::bool_switch()->default_value(false), "activate debug print");
    po::variables_map vm;
    try {
      po::store(po::parse_command_line(argc, argv, desc), vm);

      if (vm.count("help")) {
        std::cout << "Commands:" << std::endl << desc << std::endl;
        return 0;
      }

      if (vm.count("listeners")) {
        listeners = vm["listeners"].as<std::vector<std::string>>();
      }

      if (vm.count("pgn")) {
        parser::Parser parser(vm["pgn"].as<std::string>(), listeners);
        return parser.parse(false);
      }

      if (vm.count("port")) {
        port = vm["port"].as<unsigned>();
        Server engine(port, listeners);
        engine.debug_set(vm["debug"].as<bool>());
        engine.start();
        engine.play();
      }

      if (!(vm.count("pgn") || vm.count("port"))) {
        std::cerr << "No port or pgn file given" << std::endl;
        return 0;
      }
      po::notify(vm);
    }
    catch(po::error& e) {
      std::cerr << "ERROR: " << e.what() << std::endl;
      return 1;
    }
  }
  catch(std::exception& e) {
    std::cerr << e.what() << std::endl;
  }
  return 0;
}
