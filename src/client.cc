#include <iostream>
#include "client.hh"
#include <string>

Client::Client(const std::string& ip,  const std::string& port,
    const std::string& login)
    : server_(network_api::ClientNetworkAPI(ip, port))

{
  board_ = plugin::ChessBoard();
  color_black_ = server_.acknowledge(login);
  std::cout << "color: " << ((color_black_) ? "black" : "white") << std::endl;
}

void Client::init()
{
  std::string response;
  if (!get_other_move(response))
    exit (0);
  if (response.compare("uci"))
  {
    std::cerr << "Invalid protocol server" << std::endl;
    exit(0);
  }
  server_.send("uciok");
  if (!get_other_move(response))
    exit (0);
  std::cout << response << std::endl;
  if (response.compare("isready"))
  {
    std::cerr << "Invalid response protocol server" << std::endl;
    exit(0);
  }
  server_.send("readyok");
}

void Client::play()
{
  std::string response;
  if (!get_other_move(response))
    return ;
  if (response.compare("ucinewgame"))
  {
    std::cerr << "Invalid start protocol server" << std::endl;
    return ;
  }
  if (color_black_)
    if (!get_other_move_comm(response))
      return ;
  std::string move;
  bool first = true;
  while (1)
  {
    if (!get_other_move(response))
      return ;
    if (response.compare("go"))
      return ;
    if (!color_black_ && first) {
      first = false;
      board_.print();
    }

    std::cout << "> ";
    std::cin >> move;
    while (!exec_move(move, color_black_)) {
      move.clear();
      std::cout << "Move Failed: " << move << std::endl;
      std::cout << "Try another one" << std::endl << "> ";
      std::cin >> move;
    }
    board_.print();
    server_.send("bestmove " + move);
    if (!get_other_move_comm(response))
      return ;
  }
}

bool Client::exec_move(std::string move, bool color) {
  char file1;
  char file2;
  int rank1;
  int rank2;
  char promo;

  auto save = board_.clone();

  std::istringstream smove(move);
  smove >> file1 >> rank1 >> file2 >> rank2;

  auto pos2 = plugin::Position((plugin::File)(file2 - 32 - 'A'), (plugin::Rank)(rank2 - 1));

  auto ret = save.move((plugin::Color)(color),
             plugin::Position((plugin::File)(file1 - 32 - 'A'), (plugin::Rank)(rank1 - 1)),
             pos2);

  if (ret == plugin::move_ret::FAIL)
    return false;

  board_.move((plugin::Color)(color),
             plugin::Position((plugin::File)(file1 - 32 - 'A'), (plugin::Rank)(rank1 - 1)),
             pos2);

  if (ret == plugin::move_ret::PROMOTE || ret == plugin::move_ret::PT) {
    smove >> promo;
    board_.promote(pos2, (plugin::PieceType)(promo), (plugin::Color)(color));
  }

  return true;
}

bool Client::get_other_move(std::string& res)
{
  try
  {
    res = server_.receive();
    return true;
  }
  catch (std::exception)
  {
    return false;
  }
}

bool Client::get_other_move_comm(std::string& res)
{
  try
  {
    std::string tmp = "";
    std::cout << "Waiting for adversary move ...." << std::endl;
    tmp = server_.receive();
    std::cout << "Adversary moved: " << tmp.substr(tmp.size() - 5) << std::endl;
    exec_move(tmp.substr(tmp.size() - 5), !color_black_);
    board_.print();
    res = tmp;
    return true;
  }
  catch (std::exception)
  {
    return false;
  }
}
