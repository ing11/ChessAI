#pragma once

#include "network-api/server-network-api.hh"
#include <chessboard.hh>
#include <listener.hh>
#include <map>

class Server
{
  public:
    Server(unsigned short port, std::vector<std::string>& libs);
    ~Server();
    bool start();
    void play();
    void debug_set(bool debug) {debug_ = debug;}
  private:
    bool get_move(plugin::Color color, std::string& res);
    bool check_move(plugin::Color color, std::string& move);
    bool check_state(plugin::Color color);
    bool insert_save();
    network_api::ServerNetworkAPI player1_;
    network_api::ServerNetworkAPI player2_;
    plugin::ChessBoard board_;
    std::string msg_ = "position startpos moves";
    std::vector<void*> libraries_;
    std::vector<plugin::Listener*> listeners_;
    std::map<std::pair<uint64_t, uint64_t>, uint8_t> saves_;
    bool debug_ = false;
};
