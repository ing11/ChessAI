#include <boost/asio/io_service.hpp>
#include <boost/bind.hpp>
#include <boost/thread/thread.hpp>
#include <ctime>

#include "ai.hh"

#define INFINI 10000
#define NB_THREAD 5

void AI::next_move_thread(std::vector<std::pair<int, std::string>>& vec,
    int index, int best, plugin::Position& start, plugin::Position& end)
{
  //std::cout << std::endl << std::endl<< "THREAD TO MOVE: " << debug_ << std::endl << std::endl;
  //std::cout << "start: " << index << std::endl;
  plugin::ChessBoard new_board = board_.clone();
  auto prom = new_board.move((plugin::Color)((int)(color_black_)), start, end);
  int val = 0;
  vec[index].first = -INFINI;
  char bonus = '\0';
  if (prom == plugin::move_ret::PROMOTE || prom == plugin::move_ret::PT)
    val = promote_move(new_board, end, best, INFINI, tree_size_, color_black_,
        bonus);
  else if (check_draw(new_board)
      || new_board.check_draw((plugin::Color)(!color_black_)))
    val = 0;
  else
    val = -alphabeta(new_board, -INFINI, -best, tree_size_ /* must be odd */,
        !color_black_, bonus);

  if (val > vec[index].first)
  {
    vec[index].first = val;
    vec[index].second = ((int)(start.file_get()) + 'a');
    vec[index].second += ((int)(start.rank_get()) + '1');
    vec[index].second += ((int)(end.file_get()) + 'a');
    vec[index].second += ((int)(end.rank_get()) + '1');
    if ((prom == plugin::move_ret::PROMOTE) || (prom == plugin::move_ret::PT))
      vec[index].second += bonus;
  }
  //locks_[index].unlock();
  runnings_--;
  if (!runnings_)
    cv_.notify_all();
}

  AI::AI(std::string& ip, std::string& port, std::string& login)
: server_(network_api::ClientNetworkAPI(ip, port)),
  work_(io_service_),
  tree_size_(3)
{
  board_ = plugin::ChessBoard();
  color_black_ = server_.acknowledge(login);
  for (int i = 0; i < NB_THREAD; i++)
    threadpool_.create_thread(
        boost::bind(&boost::asio::io_service::run, &io_service_));
}

AI::~AI()
{
  io_service_.stop();
}

void AI::init()
{
  std::string response;
  if (!get_other_move(response))
    exit (0);
  if (response.compare("uci"))
  {
    std::cerr << "Invalid protocol server" << std::endl;
    exit(0);
  }
  server_.send("uciok");
  if (!get_other_move(response))
    exit (0);
  if (response.compare("isready"))
  {
    std::cerr << "Invalid response protocol server" << std::endl;
    exit(0);
  }
  server_.send("readyok");
}

void AI::play()
{
  /* CLOCK CALCULATION */
  //std::clock_t start = std::clock();
  //std::clock_t laps;
  //std::clock_t end;
  //std::clock_t delta;
  //double time_in_sec = 0.0;
  /*********************/
  std::string response;
  if (!get_other_move(response))
    return ;
  if (response.compare("ucinewgame"))
  {
    std::cerr << "Invalid start protocol server" << std::endl;
    return ;
  }
  if (color_black_)
    if (!get_other_move_comm(response))
      return ;
  bool first = true;
  int turn = 1;
  while (1)
  {
    turn++;
    if (!get_other_move(response))
      break ;
    if (response.compare("go"))
      break ;
    if (!color_black_ && first) {
      first = false;
    }
    // tree_size computing
    int nb_piece = board_.num_piece_get((plugin::Color)color_black_);
    int nb_move = board_.get_moves((plugin::Color)color_black_).size();
    if (turn >= 17) {
      tree_size_ = nb_move / (nb_piece) > 0 && nb_move <= 20 ? 5 : 3;
      /*if (time_in_sec < 1.0 && tree_size_ == 5
          && nb_piece + board_.num_piece_get((plugin::Color)!color_black_) <= 11)
        tree_size_ = 7;*/
    }
    //std::cout << "TREE SIZE: " << tree_size_ << std::endl;
    //////////////////////
    /* START CLOCK */
    //laps = std::clock();

    std::string move = next_move();
    server_.send("bestmove " + move);

    //delta = std::clock() - laps;
    //time_in_sec = delta / (double) CLOCKS_PER_SEC;
    //std::cout << "TIME TO MOVE: " << time_in_sec << std::endl;
    /* END CLOCK */
    exec_move(move, color_black_);
    if (!get_other_move_comm(response))
      break ;
  }
  //end = std::clock();
  //std::cout << "GAME PLAYED IN: " << (end - start) / (double) CLOCKS_PER_SEC << " / 300 secondes" << std::endl;
}

  int
AI::alphabeta(const plugin::ChessBoard& board, int alpha, int beta, int count,
    bool turn, char& bonus)
{
  /* si P est une feuille alors
     retourner la valeur de P
     sinon
     Meilleur = –INFINI
     pour tout enfant Pi de P faire
     Val = -ALPHABETA(Pi,-B,-A)
     si Val > Meilleur alors
     Meilleur = Val
     si Meilleur > A alors
     A = Meilleur
     si A ≥ B alors
     retourner Meilleur
     finsi
     finsi
     finsi
     finpour
     retourner Meilleur
     finsi*/
  if (!count)
    return board.evaluate((plugin::Color)((int)(color_black_)));
  count--;
  int best = -INFINI;
  auto tab = board.get_moves((plugin::Color)((int)(turn)));
  if (!tab.size())
    return (turn != color_black_)
      ? INFINI : -INFINI;
  for (auto& i : tab)
  {
    plugin::ChessBoard new_board = board.clone();
    auto prom = new_board.move((plugin::Color)((int)(turn)), i.first, i.second);
    int val = 0;
    if (prom == plugin::move_ret::PROMOTE || prom == plugin::move_ret::PT)
      val = promote_move(new_board, i.second, alpha, beta, count, turn, bonus);
    else if (check_draw(new_board)
        || new_board.check_draw((plugin::Color)(!turn)))
      val = 0;
    else
      val = -alphabeta(new_board, -beta, -alpha, count, !turn, bonus);

    if (val > best)
    {
      best = val;
      if (best > alpha)
      {
        alpha = best;
        if (alpha >= beta)
          return best;
      }
    }
  }
  return best;
}

std::string AI::next_move()
{
  int best = -INFINI;
  std::string move;
  std::vector<std::pair<int, std::string>> vec;
  int nb = 0;
  auto tab = board_.get_moves((plugin::Color)((int)(color_black_)));
  vec.resize(tab.size());
  for (auto& i : vec)
    i.first = -INFINI;
  runnings_ = tab.size();
  std::unique_lock<std::mutex> lock(mutex_);
  for (auto& i : tab)
  {
    //locks_[nb].lock();
    //next_move_thread(vec, nb++, best, i.first, i.second);
    io_service_.post(boost::bind(&AI::next_move_thread, boost::ref(*this),
          boost::ref(vec), nb++, best, i.first, i.second));
    /*continue;
      nb = 0;
      next_move_thread(boost::ref(vec), 2, best, i.first, i.second);
      for (int i = 0; i < 2; i++)
      {
      locks_[i].lock();
      locks_[i].unlock();
      }
      for (auto ret : vec)
      if (ret.first > best)
      {
      best = ret.first;
      move = ret.second;
      }*/
  }
  /*for (int i = 0; i < tab.size(); i++)
  {
    locks_[i].lock();
    locks_[i].unlock();
  }*/
  if (runnings_ != 0)
    cv_.wait(lock);
  for (auto i : vec)
    if (i.first > best)
    {
      best = i.first;
      move = i.second;
    }
  return move;
}

bool AI::check_draw(const plugin::ChessBoard& board) {
  auto pair = std::make_pair(board.get_Wpos(), board.get_Bpos());
  if (saves_.find(pair) != saves_.end()) {
    return saves_[pair] >= 1;
  }
  return false;
}

int AI::promote_move(plugin::ChessBoard& board, plugin::Position& pos,
    int alpha, int beta, int count, bool turn, char& bonus)
{
  constexpr std::array<std::pair<plugin::PieceType, char>, 4> array =
  {
    std::make_pair(plugin::PieceType::QUEEN, 'Q'),
    std::make_pair(plugin::PieceType::ROOK, 'R'),
    std::make_pair(plugin::PieceType::KNIGHT, 'N'),
    std::make_pair(plugin::PieceType::BISHOP, 'B')
  };
  int best = -INFINI;
  for (auto i : array)
  {
    board.promote(pos, i.first, (plugin::Color)((int)(turn)));
    int val;
    if (check_draw(board)
        || board.check_draw((plugin::Color)(!turn)))
      val = 0;
    else
      val = -alphabeta(board, -beta, -alpha, count, !turn, bonus);
    if (val > best)
    {
      best = val;
      bonus = i.second;
      if (best > alpha)
      {
        alpha = best;
        if (alpha >= beta)
          return best;
      }
    }
  }
  return best;
}


bool AI::exec_move(std::string move, bool color) {
  char file1;
  char file2;
  char rank1;
  char rank2;
  char promo;
  std::cout << move << std::endl;//////////

  std::istringstream smove(move);
  smove >> file1 >> rank1 >> file2 >> rank2;

  auto pos1 = plugin::Position((plugin::File)(file1 - 'a'), (plugin::Rank)(rank1 - '1'));
  auto pos2 = plugin::Position((plugin::File)(file2 - 'a'), (plugin::Rank)(rank2 - '1'));

  auto ret = board_.move((plugin::Color)(color), pos1, pos2);
  board_.print();/////////////

  if (ret == plugin::move_ret::PROMOTE || ret == plugin::move_ret::PT) {
    smove >> promo;
    board_.promote(pos2, (plugin::PieceType)(promo), (plugin::Color)(color));
  }
  if (ret == plugin::move_ret::FAIL)
    return false;

  if (ret == plugin::move_ret::TAKEN || ret == plugin::move_ret::PT
      || (ret == plugin::move_ret::SUCC && board_[pos2]->first == plugin::PieceType::PAWN))
    saves_.clear();
  auto pair = std::make_pair(board_.get_Wpos(), board_.get_Bpos());
  if (saves_.find(pair) != saves_.end())
    saves_[pair]++;
  else
    saves_.emplace(pair, 1);
  return true;
}

bool AI::get_other_move(std::string& res)
{
  try
  {
    res = server_.receive();
    return true;
  }
  catch (std::exception)
  {
    return false;
  }
}

bool AI::get_other_move_comm(std::string& res)
{
  try
  {
    std::string tmp = "";
    tmp = server_.receive();
    exec_move(tmp.substr(tmp.size() - 5), !color_black_);
  //  std::cout << tmp.substr(tmp.size() - 6) << std::endl;
  //  board_.print();
    res = tmp;
    return true;
  }
  catch (std::exception)
  {
    return false;
  }
}
