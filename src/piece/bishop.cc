#include "bishop.hh"

namespace plugin
{

    PieceType Bishop::get_type()
    {
      return PieceType::BISHOP;
    }

    uint64_t Bishop::get_mvbitb(uint64_t empty)
    {
      uint64_t res = 0;
      uint64_t pos = get_pos();
      while (pos)
      {
        uint64_t pos1 = pos & -pos;
        pos ^= pos1;

        res |= NEray(pos1, ~empty);
        res |= NWray(pos1, ~empty);
        res |= SEray(pos1, ~empty);
        res |= SWray(pos1, ~empty);
      }

      return res & empty;
    }

    uint64_t Bishop::get_capbitb(uint64_t en, uint64_t empty)
    {
      uint64_t res = 0;
      uint64_t pos = get_pos();
      while (pos)
      {
        uint64_t pos1 = pos & -pos;
        pos ^= pos1;

        res |= NEray(pos1, ~empty);
        res |= NWray(pos1, ~empty);
        res |= SEray(pos1, ~empty);
        res |= SWray(pos1, ~empty);
      }

      return res & en;
    }
}
