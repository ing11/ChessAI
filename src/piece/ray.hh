#pragma once
#include <cstdint>

namespace plugin
{

  uint64_t Eray(uint64_t p, uint64_t obs);


  uint64_t Wray(uint64_t p, uint64_t obs);

  uint64_t NEray(uint64_t p, uint64_t obs);

  uint64_t NWray(uint64_t p, uint64_t obs);


  uint64_t SEray(uint64_t p, uint64_t obs);

  uint64_t SWray(uint64_t p, uint64_t obs);

  uint64_t Nray(uint64_t p, uint64_t obs);

  uint64_t Sray(uint64_t p, uint64_t obs);
}
