#include "knight.hh"

namespace plugin
{
    PieceType Knight::get_type()
    {
      return PieceType::KNIGHT;
    }

    uint64_t Knight::get_mvbitb(uint64_t empty)
    {
      uint64_t res = 0;
      uint64_t pos = get_pos();
      uint64_t notAFile = 0xfefefefefefefefe;
      uint64_t notABFile = 0xfcfcfcfcfcfcfcfc;
      uint64_t notHFile = 0x7f7f7f7f7f7f7f7f;
      uint64_t notGHFile = 0x3f3f3f3f3f3f3f3f;

      res = (pos << 6) & notGHFile;
      res |= (pos << 10) & notABFile;
      res |= (pos >> 6) & notABFile;
      res |= (pos >> 10) & notGHFile;
      res |= (pos << 15) & notHFile;
      res |= (pos << 17) & notAFile;
      res |= (pos >> 15) & notAFile;
      res |= (pos >> 17) & notHFile;
      return res & empty;
    }


    uint64_t Knight::get_capbitb(uint64_t en, uint64_t empty)
    {
      empty++;
      return get_mvbitb(en);
    }

}
