#include "king.hh"

namespace plugin
{
    PieceType King::get_type()
    {
      return PieceType::KING;
    }

    uint64_t King::get_mvbitb(uint64_t empty)
    {
      uint64_t res = 0;
      uint64_t pos = get_pos();
      uint64_t notAFile = 0xfefefefefefefefe;
      uint64_t notHFile = 0x7f7f7f7f7f7f7f7f;

      res = (pos << 7) & notHFile;
      res |= (pos << 9) & notAFile;
      res |= (pos >> 7) & notAFile;
      res |= (pos >> 9) & notHFile;
      res |= (pos << 8);
      res |= (pos << 1) & notAFile;
      res |= (pos >> 8);
      res |= (pos >> 1) & notHFile;
      if (!mov_)
      {
        if ((pos >> 1) & empty)
          res |= (pos >> 2);
        if ((pos << 1) & empty)
          res |= (pos << 2);
      }
      return res & empty;
    }

    uint64_t King::get_capbitb(uint64_t en, uint64_t empty)
    {
      empty++;
      return get_mvbitb(en);
    }

    bool King::move(Position b, Position e, uint64_t empty, uint64_t en)
    {
      bool boo = false;
      uint64_t tmp = pos_;
      pos_ = PositionToU64(b.file_get(), b.rank_get());


      if (check_mv(e, empty) || check_cap(e, en, empty))
      {
        pos_ = tmp;
        mov_ = true;
        pos_ ^=  PositionToU64(b.file_get(), b.rank_get());
        pos_ |=  PositionToU64(e.file_get(), e.rank_get());
        boo = true;
      }
      else
        pos_ = tmp;
      return boo;
    }

    bool King::is_moved() {return mov_;}


}
