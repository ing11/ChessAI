#pragma once

#include <cstdint>
#include <cmath>
#include "../plugin/color.hh"
#include "../plugin/piece-type.hh"
#include "../plugin/position.hh"

namespace plugin
{

 uint64_t PositionToU64 (File file, Rank rank);
 Position U64ToPos (uint64_t p);




class Piece
{
  public:
    Piece (uint64_t p, Color pc) : pos_(p), pc_(pc)
    {}

    virtual ~Piece() = default;

    virtual PieceType get_type() = 0;

    virtual uint64_t get_mvbitb(uint64_t empty) = 0;

    virtual uint64_t get_capbitb(uint64_t en, uint64_t empty) = 0;

    virtual bool move(Position b, Position e, uint64_t empty, uint64_t en);

    uint64_t get_pos();

    void set_pos(uint64_t pos);

    void cap_atpos(Position p);

    bool check_atpos(Position p);

    uint64_t get_mvsingle(uint64_t single, uint64_t empty);

    uint64_t get_capsingle(uint64_t single, uint64_t en, uint64_t empty);

  protected:
    bool check_mv(Position p, uint64_t empty);
    bool check_cap(Position p, uint64_t en, uint64_t empty);


    uint64_t pos_;
    Color pc_;
};

}
