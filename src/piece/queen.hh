#pragma once

#include "piece.hh"
#include "ray.hh"

namespace plugin
{

class Queen : public Piece
{
  public:
    Queen (uint64_t p, Color pc) : Piece(p, pc)
    {}
    ~Queen(){}
    PieceType get_type() override;

    uint64_t get_mvbitb(uint64_t empty) override;

    uint64_t get_capbitb(uint64_t en, uint64_t empty) override;

};

}
