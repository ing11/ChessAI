#pragma once

#include "piece.hh"

struct g_global
{
  uint64_t pawn2 = 0;
  uint64_t tempo = 0;
  bool prem = false;
};


namespace plugin
{

class Pawn : public Piece
{
  public:
    Pawn (uint64_t p, Color pc) : Piece(p, pc)
    {}
    ~Pawn(){}
    PieceType get_type() override;

    uint64_t get_mvbitb(uint64_t empty) override;

    uint64_t get_capbitb(uint64_t en, uint64_t empty) override;

    bool move(Position b, Position e, uint64_t empty, uint64_t en) override;


};

}
