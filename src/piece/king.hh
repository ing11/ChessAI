#pragma once

#include "piece.hh"

namespace plugin
{

class King : public Piece
{
  public:
    King (uint64_t p, Color pc) : Piece(p, pc), mov_(false)
    {}
    King (uint64_t p, Color pc, bool mov) : Piece(p, pc), mov_(mov)
    {}
    ~King(){}
    PieceType get_type() override;

    uint64_t get_mvbitb(uint64_t empty) override;

    uint64_t get_capbitb(uint64_t en, uint64_t empty) override;

    bool move(Position b, Position e, uint64_t empty, uint64_t en) override;

    bool is_moved();

  private:
    bool mov_;

};

}
