#pragma once

#include "piece.hh"

namespace plugin
{

class Knight : public Piece
{
  public:
    Knight (uint64_t p, Color pc) : Piece(p, pc)
    {}
    ~Knight(){}
    PieceType get_type() override;

    uint64_t get_mvbitb(uint64_t empty) override;

    uint64_t get_capbitb(uint64_t en, uint64_t empty) override;

};

}
