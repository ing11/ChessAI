#include "chessboard.hh"

namespace plugin
{
  ChessBoard::ChessBoard()
  {
    white_.push_back(new Pawn(0x000000000000FF00, Color::WHITE));
    white_.push_back(new Knight(PositionToU64(File::B, Rank::ONE)
          | PositionToU64(File::G, Rank::ONE), Color::WHITE));
    white_.push_back(new Bishop(PositionToU64(File::C, Rank::ONE)
          | PositionToU64(File::F, Rank::ONE), Color::WHITE));
    white_.push_back(new Rook(PositionToU64(File::A, Rank::ONE)
          | PositionToU64(File::H, Rank::ONE), Color::WHITE));
    white_.push_back(new Queen(PositionToU64(File::D, Rank::ONE), Color::WHITE));
    white_.push_back(new King(PositionToU64(File::E, Rank::ONE), Color::WHITE));

    black_.push_back(new Pawn(0x00FF000000000000, Color::BLACK));
    black_.push_back(new Knight(PositionToU64(File::B, Rank::EIGHT)
          | PositionToU64(File::G, Rank::EIGHT), Color::BLACK));
    black_.push_back(new Bishop(PositionToU64(File::C, Rank::EIGHT)
          | PositionToU64(File::F, Rank::EIGHT), Color::BLACK));
    black_.push_back(new Rook(PositionToU64(File::A, Rank::EIGHT)
          | PositionToU64(File::H, Rank::EIGHT), Color::BLACK));
    black_.push_back(new Queen(PositionToU64(File::D, Rank::EIGHT), Color::BLACK));
    black_.push_back(new King(PositionToU64(File::E, Rank::EIGHT), Color::BLACK));
  }

  ChessBoard::ChessBoard(std::vector<Piece*> white, std::vector<Piece*> black,
      bool wmov, bool bmov, bool wmov_k, bool wmov_q, bool bmov_k, bool bmov_q)
  : white_(white), black_(black), wmov_(wmov), bmov_(bmov), wmov_k_(wmov_k),
  wmov_q_(wmov_q), bmov_k_(bmov_k), bmov_q_(bmov_q)
  {}

  ChessBoard::~ChessBoard()
  {
    for (auto it = white_.begin(); it != white_.end(); it++)
      delete (*it);
    for (auto it = black_.begin(); it != black_.end(); it++)
      delete (*it);
  }

  uint64_t ChessBoard::get_Wpos() const
  {
    uint64_t res = 0;
    for (auto it = white_.begin(); it != white_.end(); it++)
      res |= (*it)->get_pos();
    return res;
  }

  uint64_t ChessBoard::get_Bpos() const
  {
    uint64_t res = 0;
    for (auto it = black_.begin(); it != black_.end(); it++)
      res |= (*it)->get_pos();
    return res;
  }

  uint64_t ChessBoard::get_atk(Color c) const
  {
    std::vector<Piece*> tmp;
    uint64_t en = 0;
    uint64_t empty = get_empty();
    if (c == Color::BLACK)
    {
      tmp = black_;
      en = get_Wpos();
    }
    else
    {
      tmp = white_;
      en = get_Bpos();
    }
    uint64_t res = 0;
    for (auto it = tmp.begin(); it != tmp.end(); it++)
      res |= (*it)->get_capbitb(en, empty);
    return res;

  }

  uint64_t ChessBoard::get_empty() const
  {
    return ~(get_Wpos() | get_Bpos());
  }

  void ChessBoard::cap(Position cap, Color c)
  {
    std::vector<Piece*> tmp;
    if (c == Color::WHITE)
      tmp = black_;
    else
      tmp = white_;
    for (auto it = tmp.begin(); it != tmp.end(); it++)
      if ((*it)->check_atpos(cap))
        (*it)->cap_atpos(cap);
  }

  bool ChessBoard::cast_k(Color c)
  {
    Position start(File::A, Rank::ONE);
    Position end(File::A, Rank::ONE);
    std::vector<Piece*> tmp;
    uint64_t empty = get_empty();
    uint64_t full = 0;
    if (c == Color::BLACK)
    {
      tmp = black_;
      start = Position(File::H, Rank::EIGHT);
      end = Position(File::F, Rank::EIGHT);
    }
    else
    {
      tmp = white_;
      start = Position(File::H, Rank::ONE);
      end = Position(File::F, Rank::ONE);
    }
    empty ^= tmp[5]->get_pos();
    return tmp[3]->move(start, end, empty, full);
  }

  bool ChessBoard::cast_q(Color c)
  {
    Position start(File::A, Rank::ONE);
    Position end(File::A, Rank::ONE);
    std::vector<Piece*> tmp;
    uint64_t full = 0;
    uint64_t empty = get_empty();
    if (c == Color::BLACK)
    {
      tmp = black_;
      start = Position(File::A, Rank::EIGHT);
      end = Position(File::D, Rank::EIGHT);
    }
    else
    {
      tmp = white_;
      start = Position(File::A, Rank::ONE);
      end = Position(File::D, Rank::ONE);
    }
    empty ^= tmp[5]->get_pos();
    return tmp[3]->move(start, end, empty, full);
  }


  move_ret ChessBoard::move(Color c, Position begin, Position end)
  {
    std::vector<Piece*> tmp;
    uint64_t en = 0;
    uint64_t mask = 0;
    bool succ = false;
    bool caput = false;
    bool promot = false;
    bool cqs = false;
    bool cks = false;
    bool is_pawn = false;
    bool pass = false;
    bool is_check = check_check(c);
    g_global.prem = false;
    if (c == Color::BLACK)
    {
      tmp = black_;
      en = get_Wpos();
      mask = 0x00000000000000FF;
    }
    else
    {
      tmp = white_;
      en = get_Bpos();
      mask = 0xFF00000000000000;
    }
    for (auto it = tmp.begin(); it != tmp.end() && !succ; it++)
    {
      if ((*it)->check_atpos(begin) && ((*it)->get_type() == PieceType::KING))
      {
        succ = (*it)->move(begin, end, get_empty() & ~get_atk((Color)!(bool)c), en);
        if (succ && (begin.file_get() == File::E) && (end.file_get() == File::G))
        {
          cks = cast_k(c);
          succ = cks;
          if (is_check)
            return move_ret::FAIL;
        }
        else if (succ && (begin.file_get() == File::E) && (end.file_get() == File::C))
        {
          cqs = cast_q(c);
          succ = cqs;
          if (is_check)
            return move_ret::FAIL;
        }
        if (succ && c == Color::BLACK)
          bmov_ = true;
        else if (succ)
          wmov_ = true;

      }
      else if ((*it)->check_atpos(begin))
      {
        if ((*it)->get_type() == PieceType::ROOK)
        {
          if (c == Color::BLACK)
          {
            if (begin.file_get() == File::H)
              bmov_k_ = true;
            if (begin.file_get() == File::A)
              bmov_q_ = true;
          }
          else
          {
            if (begin.file_get() == File::H)
              wmov_k_ = true;
            if (begin.file_get() == File::A)
              wmov_q_ = true;
          }
        }
        is_pawn = PieceType::PAWN == (*it)->get_type();
        if (is_pawn)
          succ = (*it)->move(begin, end, get_empty(), en|g_global.tempo);
        else
          succ = (*it)->move(begin, end, get_empty(), en);
      }
    }
    if (en & PositionToU64(end.file_get(), end.rank_get()))
    {
      caput = true;
      cap(end, c);
    }
    if (tmp[0]->get_pos() & mask)
    {
      tmp[0]->set_pos(tmp[0]->get_pos() & ~mask);
      promot = true;
    }

    if (g_global.pawn2 && !g_global.prem)
    {
      if (is_pawn && g_global.tempo & PositionToU64(end.file_get(), end.rank_get()))
      {
        cap(U64ToPos(g_global.pawn2), c);
        pass = true;
      }
      else
        cap(U64ToPos(g_global.tempo), c);
      g_global.pawn2 = 0;
      g_global.tempo = 0;
    }

    if (check_check(c) || !succ)
      return move_ret::FAIL;
    else if (cks)
      return move_ret::CAST_KING;
    else if (cqs)
      return move_ret::CAST_QUEEN;
    else if (pass)
      return move_ret::PASS;
    else if (caput && promot)
      return move_ret::PT;
    else if (promot)
      return move_ret::PROMOTE;
    else if (caput)
      return move_ret::TAKEN;
    else
      return move_ret::SUCC;
  }

  bool ChessBoard::check_check(Color c) const// Color of king in check
  {
    std::vector<Piece*> tmp;
    uint64_t en = 0;
    uint64_t empty = get_empty();
    uint64_t kingpos = 0;
    uint64_t cap = 0;
    if (c == Color::BLACK)
    {
      tmp = white_;
      en = get_Bpos();
      kingpos = black_[5]->get_pos();
    }
    else
    {
      tmp = black_;
      en = get_Wpos();
      kingpos = white_[5]->get_pos();
    }
    for (auto it = tmp.begin(); it != tmp.end(); it++)
    {
      cap |= (*it)->get_capbitb(en, empty);
      //printU64(cap);
    }

    return (kingpos & cap);
  }

  uint64_t ChessBoard::get_validmove(Color c) const
  {
    std::vector<Piece*> k;
    std::vector<Piece*> e;
    uint64_t en;
    uint64_t obs;
    uint64_t res = 0;
    if (c == Color::BLACK)
    {
      k = black_;
      e = white_;
      en = get_Wpos();
      obs = get_Bpos();
    }
    else
    {
      k = white_;
      e = black_;
      en = get_Bpos();
      obs = get_Wpos();
    }
    for (auto p = k.begin(); p != k.end(); p++)
    {
      uint64_t curp = (*p)->get_pos();
      uint64_t save = (*p)->get_pos();
      while (curp)
      {
        uint64_t single = curp & -curp;
        curp ^= single;
        obs ^= single;
        (*p)->set_pos(single);
        uint64_t posmv = (*p)->get_capbitb(en, ~(obs | en))
          | (*p)->get_mvbitb(~(obs | en));
        while (posmv)
        {
          uint64_t cap = 0;
          uint64_t curmv = posmv & -posmv;
          posmv ^= curmv;
          obs |= curmv;

          if ((*p)->get_type() == PieceType::KING)
            (*p)->set_pos(curmv);
          for (auto it = e.begin(); it != e.end(); it++)
            cap |= (*it)->get_capbitb(obs, ~(en | obs));
          if (!(k[5]->get_pos() & cap))
            res |= curmv;
          obs ^= curmv;
        }
        obs |= single;
      }
      (*p)->set_pos(save);
    }
    return res;
  }

  bool ChessBoard::check_checkmate_or_draw(Color c) const
  {
    return !(get_validmove(c));
  }

  bool ChessBoard::check_draw(Color c) const
  {
    return (check_checkmate_or_draw(c) && !check_check(c));
  }

  bool ChessBoard::check_checkmate(Color c) const // Color of king in check
  {
    return (check_checkmate_or_draw(c) && check_check(c));
  }

  ChessBoard::opt_piece_t ChessBoard::operator[](const Position& position) const
  {
    uint64_t pos = PositionToU64(position.file_get(), position.rank_get());
    side_piece_t res;

    if (pos & get_Bpos())
    {
      res.second = Color::BLACK;
      for (auto it = black_.begin(); it != black_.end(); it++)
        if ((*it)->get_pos() & pos)
          res.first = (*it)->get_type();
    }
    else if (pos & get_Wpos())
    {
      res.second = Color::WHITE;
      for (auto it = white_.begin(); it != white_.end(); it++)
        if ((*it)->get_pos() & pos)
          res.first = (*it)->get_type();
    }
    else
      return std::experimental::nullopt;
    opt_piece_t ret = res;
    return ret;
  }

  void ChessBoard::print()
  {
    char* board = new char[8*8]();
    for (auto it = white_.begin(); it != white_.end(); it++)
    {
      uint64_t curpos = (*it)->get_pos();
      while (curpos)
      {
        uint64_t single = curpos & -curpos;
        curpos ^= single;
        board[(int)std::log2(single)] = ((char)(*it)->get_type());
      }
    }

    for (auto it = black_.begin(); it != black_.end(); it++)
    {
      uint64_t curpos = (*it)->get_pos();
      while (curpos)
      {
        uint64_t single = curpos & -curpos;
        curpos ^= single;
        board[(int)std::log2(single)] = ((char)(*it)->get_type()) - 'A' + 'a';
      }
    }

    std::string black = "40";
    std::string white = "40";

    std::cout << "\033[0;36m" << "   a b c d e f g h" << "\033[0m" << std::endl;
    for (int i = 0; i < 8; i++)
    {
      std::cout << "\033[0;36m" << 8 - i << "\033[0m" <<  " |";
      for(int j = 0; j < 8; j++)
      {
        if (board[(7-i)*8 + j] >= 'a' && board[(7-i)*8 + j] <= 'z')
          std::cout << "\033[1;31;" << ((i + j) % 2 == 0 ? black : white)  << "m" << board[(7-i)*8 + j] << "\033[0m" << '|';
        else if (board[(7-i)*8 + j] >= 'A' && board[(7-i)*8 + j] <= 'Z')
          std::cout << "\033[1;37;" << ((i + j) % 2 == 0 ? black : white)  << "m" << board[(7-i)*8 + j] << "\033[0m" << '|';
        else
          std::cout << "\033[0;" << ((i + j) % 2 == 0 ? black : white) << "m " << "\033[0m|";
      }
      std::cout << " " << "\033[0;36m" << 8 - i << "\033[0m" << std::endl;
    }
    std::cout << "\033[0;36m" << "   a b c d e f g h" << "\033[0m" << std::endl;
    delete [] board;
  }

  void ChessBoard::printU64(uint64_t u) const
  {
    char* board = new char[8*8]();
    uint64_t curpos = u;
    while (curpos)
    {
      uint64_t single = curpos & -curpos;
      curpos ^= single;
      board[(int)std::log2(single)] = '1';
    }

    for (int i = 0; i < 8; i++)
    {
      for(int j = 0; j < 8; j++)
      {
        if (board[(7-i)*8 + j])
          std::cout << board[(7-i)*8 + j];
        else
          std::cout << '.';
      }
      std::cout << std::endl;
    }
    delete [] board;
  }

  void ChessBoard::promote(Position pos, PieceType to, Color c)
  {
    std::vector<Piece*> tmp;
    if (c == Color::BLACK)
      tmp = black_;
    else
      tmp = white_;

    for (auto it = tmp.begin(); it != tmp.end(); it++)
      if ((*it)->check_atpos(pos))
        (*it)->cap_atpos(pos);


    for (auto it = tmp.begin(); it != tmp.end(); it++)
      if ((*it)->get_type() == to)
        (*it)->set_pos((*it)->get_pos()
            | PositionToU64(pos.file_get(), pos.rank_get()));
  }

  int nbBitSet(uint64_t u)
  {
    int res = 0;
    while (u && ++res)
      u ^= u & -u;
    return res;
  }

  int pawn_eval(uint64_t ppos, Color c)
  {
    int res = 0;
    while (ppos)
    {
      uint64_t single = ppos & -ppos;
      ppos ^= single;
      Position test = U64ToPos(single);
      if (c == Color::WHITE)
        res += (int)(test.rank_get()) - 1;
      else
        res += 7-(int)(test.rank_get());
    }
    return res;
  }


  int ChessBoard::evaluate(Color c) const
  {
    uint64_t col = 0;
    int res = 0;
    uint64_t bpos = get_Bpos();
    uint64_t wpos = get_Wpos();
    uint64_t empty = !(bpos | wpos);
    uint64_t bvalid = get_validmove(Color::BLACK);
    uint64_t wvalid = get_validmove(Color::WHITE);
    uint64_t b_atk = get_atk(Color::BLACK);
    uint64_t w_atk = get_atk(Color::WHITE);

    int bmv = 0;
    int wmv = 0;
    int bcap = 0;
    int wcap = 0;
    if (c == Color::WHITE)
      col = 1;
    else
      col = -1;
    for (int i = 0; i < 6; i++)
    {
      int b = nbBitSet(black_[i]->get_pos());
      int w = nbBitSet(white_[i]->get_pos());
      int cb = nbBitSet(black_[i]->get_pos() & w_atk);
      int cw = nbBitSet(white_[i]->get_pos() & b_atk);
      uint64_t pw = 0;
      uint64_t pb = 0;

      if (nbBitSet(bpos | wpos) < 25 && i == 5)
      {
        res += nbBitSet(white_[5]->get_mvbitb(wvalid)
            | white_[5]->get_capbitb(bpos, wvalid))
          - nbBitSet(black_[5]->get_mvbitb(bvalid)
            | black_[5]->get_capbitb(wpos, bvalid));
      }

      if (i == 1)
      {
        bmv += 2 * nbBitSet(black_[i]->get_mvbitb(bvalid));
        bcap += 2 * nbBitSet(black_[i]->get_capbitb(wpos, bvalid));
        wmv +=  2 * nbBitSet(white_[i]->get_mvbitb(wvalid));
        wcap += 2 * nbBitSet(white_[i]->get_capbitb(bpos, wvalid));
      }
      else if (i == 4)
      {
        bmv += nbBitSet(black_[i]->get_mvbitb(bvalid))/2;
        bcap += nbBitSet(black_[i]->get_capbitb(wpos, bvalid))/2;
        wmv +=  nbBitSet(white_[i]->get_mvbitb(wvalid))/2;
        wcap += nbBitSet(white_[i]->get_capbitb(bpos, wvalid))/2;
      }
      else
      {
        bmv += nbBitSet(black_[i]->get_mvbitb(bvalid));
        bcap += nbBitSet(black_[i]->get_capbitb(wpos, bvalid));
        wmv += nbBitSet(white_[i]->get_mvbitb(wvalid));
        wcap += nbBitSet(white_[i]->get_capbitb(bpos, wvalid));
      }
      for (int k = 0; k < 6; k++)
      {
        pw |= white_[k]->get_capbitb(white_[i]->get_pos(), wvalid);
        pb |= black_[k]->get_capbitb(black_[i]->get_pos(), bvalid);
      }


      if (i < 5)
      {
        res += ((5-i) * (nbBitSet(white_[i]->get_capbitb(wpos, empty))
          - nbBitSet(black_[i]->get_capbitb(bpos, empty))))/2;
        res -= (i+1) * (cw-cb);
        res += (i+1) * (nbBitSet(pw) - nbBitSet(pb));
      }
      if (i == 0)
        res += (pawn_eval(white_[0]->get_pos(), Color::WHITE)
            - pawn_eval(black_[0]->get_pos(), Color::BLACK));

      if (i == 0)
      {
        res += 20 * (w-b);
      }
      else if (i == 1)
      {
        res += 50 * (w-b);
      }
      else if (i == 2)
      {
        res += 60 * (w-b);
      }
      else if (i == 3)
      {
        res += 80 * (w-b);
      }
      else if (i == 4)
      {
        res += 200 * (w-b);
      }
      else
        res += 500 * (w-b);
    }
    res += (wcap-bcap)/2 + (wmv-bmv)/2;
    if (check_check(Color::WHITE))
      res -= 5;
    if (check_check(Color::BLACK))
      res += 5;

    return res * col;
  }

  ChessBoard ChessBoard::clone() const
  {
    std::vector<Piece*> white;
    std::vector<Piece*> black;

    white.push_back(new Pawn(white_[0]->get_pos(), Color::WHITE));
    white.push_back(new Knight(white_[1]->get_pos(), Color::WHITE));
    white.push_back(new Bishop(white_[2]->get_pos(), Color::WHITE));
    white.push_back(new Rook(white_[3]->get_pos(), Color::WHITE, wmov_k_, wmov_q_));
    white.push_back(new Queen(white_[4]->get_pos(), Color::WHITE));
    white.push_back(new King(white_[5]->get_pos(), Color::WHITE, wmov_));

    black.push_back(new Pawn(black_[0]->get_pos(), Color::BLACK));
    black.push_back(new Knight(black_[1]->get_pos(), Color::BLACK));
    black.push_back(new Bishop(black_[2]->get_pos(), Color::BLACK));
    black.push_back(new Rook(black_[3]->get_pos(), Color::BLACK, bmov_k_, bmov_q_));
    black.push_back(new Queen(black_[4]->get_pos(), Color::BLACK));
    black.push_back(new King(black_[5]->get_pos(), Color::BLACK, bmov_));

    return ChessBoard(white, black,
        wmov_, bmov_, wmov_k_, wmov_q_, bmov_k_, bmov_q_);
  }


  std::vector<std::pair<Position, Position>> ChessBoard::get_moves(Color c) const
  {
    std::vector<Piece*> tmp;
    std::vector<std::pair<Position, Position>> res;
    //uint64_t valid;
    uint64_t en = 0;
    uint64_t empty = get_empty();
    if (c == Color::BLACK)
    {
      tmp = black_;
      en = get_Wpos();
      //valid = get_validmove(Color::BLACK);
    }
    else
    {
      tmp = white_;
      en = get_Bpos();
      //valid = get_validmove(Color::WHITE);
    }

    for (auto it = tmp.begin(); it != tmp.end(); it++)
    {
      uint64_t pos = (*it)->get_pos();
      while (pos)
      {
        uint64_t single = pos & -pos;
        pos ^= single;
        uint64_t move = ((*it)->get_mvsingle(single, empty)
          | (*it)->get_capsingle(single, en, empty)); // & valid;
        while (move)
        {
          uint64_t single_mv = move & -move;
          move ^= single_mv;
          ChessBoard test = this->clone();
          auto ret = test.move(c, U64ToPos(single), U64ToPos(single_mv));
          if (ret != move_ret::FAIL /*&& ret != move_ret::CAST_KING && ret != move_ret::CAST_QUEEN*/)
          {
            res.emplace_back(U64ToPos(single), U64ToPos(single_mv));
            /*std::cout << (char)('a' + (int) res.back().first.file_get())
              << (char)('1' + (int) res.back().first.rank_get())
              << (char)('a' + (int) res.back().second.file_get())
              << (char)('1' + (int) res.back().second.rank_get()) << ", ";*/
          }
        }
      }
    }
    //std::cout << std::endl << std::endl;
    return res;
  }

  ChessBoard ChessBoard::operator=(const ChessBoard& t)
  {
    return t.clone();
  }

  ChessBoard::ChessBoard(const ChessBoard& t)
  {
    (*this) = t.clone();
  }


}
