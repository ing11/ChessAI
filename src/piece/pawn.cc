#include "pawn.hh"

namespace plugin
{

  g_global g_global;

    PieceType Pawn::get_type()
    {
      return PieceType::PAWN;
    }

    uint64_t Pawn::get_mvbitb(uint64_t empty)
    {
      uint64_t res = 0;
      uint64_t pos = get_pos();
      if (pc_ == Color::WHITE) // White en bas
      {
        res = pos << 8;
        pos &= 0x000000000000FF00;
        if ((pos << 8) & empty)
          res |= pos << 16;
      }
      else
      {
        res = pos >> 8;
        pos &= 0x00FF000000000000;
        if ((pos >> 8) & empty)
          res |= pos >> 16;
      }
      return res & empty;
    }

    uint64_t Pawn::get_capbitb(uint64_t en, uint64_t empty)
    {
      empty++;
      uint64_t res = 0;
      uint64_t notAFile = 0xfefefefefefefefe;
      uint64_t notHFile = 0x7f7f7f7f7f7f7f7f;
      if (pc_ == Color::WHITE) // White en bas
        res = ((pos_ << 9) & notAFile) | ((pos_ << 7) & notHFile);
      else
        res = ((pos_ >> 9) & notHFile) | ((pos_ >> 7) & notAFile);
      return res & en;
    }

    bool Pawn::move(Position b, Position e, uint64_t empty, uint64_t en)
    {
      bool boo = false;
      uint64_t tmp = pos_;
      pos_ = PositionToU64(b.file_get(), b.rank_get());
      if (check_mv(e, empty) || check_cap(e, en, empty))
      {
        pos_ = tmp;
        if (std::abs((int)b.rank_get()-(int)e.rank_get()) == 2)
        {
          g_global.pawn2 = PositionToU64(e.file_get(), e.rank_get());
          if (pc_ == Color::BLACK)
            g_global.tempo = g_global.pawn2 << 8;
          else
            g_global.tempo = g_global.pawn2 >> 8;
          g_global.prem = true;
        }
        pos_ ^=  PositionToU64(b.file_get(), b.rank_get());
        pos_ |=  PositionToU64(e.file_get(), e.rank_get());
        boo = true;
      }
      else
        pos_ = tmp;
      return boo;
    }

}
