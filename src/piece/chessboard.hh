#pragma once

#include <experimental/optional>
#include <vector>
#include <utility>

#include "../plugin/chessboard-interface.hh"
#include "piece.hh"
#include "bishop.hh"
#include "rook.hh"
#include "pawn.hh"
#include "queen.hh"
#include "king.hh"
#include "knight.hh"

namespace plugin
{

  extern g_global g_global;

  enum class move_ret {
    SUCC,
    FAIL,
    CAST_KING,
    CAST_QUEEN,
    TAKEN,
    PROMOTE,
    PT,
    PASS
  };

  int nbBitSet(uint64_t u);

  class ChessBoard : public ChessboardInterface
  {
    public:
      using side_piece_t = std::pair<PieceType, Color>;
      using opt_piece_t  = std::experimental::optional<side_piece_t>;

      ChessBoard();
      ChessBoard(std::vector<Piece*> white, std::vector<Piece*> black, bool,
          bool, bool, bool, bool, bool);
      ~ChessBoard();

      ChessBoard operator=(const ChessBoard&);
      ChessBoard (const ChessBoard&);


      uint64_t get_Wpos() const;

      uint64_t get_Bpos() const;

      uint64_t get_empty() const;

      uint64_t get_atk(Color c) const;

      void cap (Position cap, Color c);

      move_ret move(Color c, Position begin, Position end);

      bool check_check(Color c) const; // Color of king in check

      uint64_t get_validmove(Color c) const;

      bool check_checkmate_or_draw(Color c) const;

      bool check_draw(Color c) const;

      bool check_checkmate(Color c) const;

      opt_piece_t operator[](const Position& position) const override;

      void print();

      void printU64(uint64_t u) const;

      void promote(Position pos, PieceType to, Color c);

      bool cast_k(Color c);

      bool cast_q(Color c);

      int evaluate(Color c) const;

      ChessBoard clone() const;

      std::vector<std::pair<Position, Position>> get_moves(Color c) const;

      uint8_t num_piece_get(Color c) {
        uint64_t side = (c == Color::WHITE) ? get_Wpos() : get_Bpos();
        uint8_t res = 0;
        while (side) {
          res += side & 0x0000000000000001;
          side = side >> 1;
        }
        return res;
      }

    private:
      std::vector<Piece*> white_;
      std::vector<Piece*> black_;
      bool wmov_ = false;
      bool bmov_ = false;
      bool wmov_k_ = false;
      bool wmov_q_ = false;
      bool bmov_k_ = false;
      bool bmov_q_ = false;
  };

}
