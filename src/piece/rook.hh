#pragma once

#include "piece.hh"

#include "ray.hh"

namespace plugin
{

class Rook : public Piece
{
  public:
    Rook (uint64_t p, Color pc) : Piece(p, pc)
    {
      mov_k = false;
      mov_q = false;
    }
    Rook (uint64_t p, Color pc, bool mov_k_, bool mov_q_)
      : Piece(p, pc)
    {
      mov_k = mov_k_;
      mov_q = mov_q_;
    }
    ~Rook(){}
    PieceType get_type() override;

    uint64_t get_mvbitb(uint64_t empty) override;

    uint64_t get_capbitb(uint64_t en, uint64_t empty) override;

    bool move(Position b, Position e, uint64_t empty, uint64_t en) override;

  private:
    bool mov_q;
    bool mov_k;

};

}
