#include "rook.hh"

namespace plugin
{

    PieceType Rook::get_type()
    {
      return PieceType::ROOK;
    }

    uint64_t Rook::get_mvbitb(uint64_t empty)
    {
      uint64_t res = 0;
      uint64_t pos = get_pos();
      while (pos)
      {
        uint64_t pos1 = pos & -pos;
        pos ^= pos1;

        res |= Eray(pos1, ~empty);
        res |= Wray(pos1, ~empty);
        res |= Nray(pos1, ~empty);
        res |= Sray(pos1, ~empty);

      }
      return res & empty;
    }


    uint64_t Rook::get_capbitb(uint64_t en, uint64_t empty)
    {
      uint64_t res = 0;
      uint64_t pos = get_pos();
      while(pos)
      {
        uint64_t pos1 = pos & -pos;
        pos ^= pos1;

        res |= Eray(pos1, ~empty);
        res |= Wray(pos1, ~empty);
        res |= Nray(pos1, ~empty);
        res |= Sray(pos1, ~empty);
      }

      return res & en;
    }

    bool Rook::move(Position b, Position e, uint64_t empty, uint64_t en)
    {

      if (!en)
      {
        if (b.file_get() == File::H && mov_k)
          return false;
        if (b.file_get() == File::A && mov_q)
          return false;
      }

      if (!check_atpos(b))
        return false;

      bool boo = false;
      uint64_t tmp = pos_;
      pos_ = PositionToU64(b.file_get(), b.rank_get());

      if (check_mv(e, empty) || check_cap(e, en, empty))
      {
        pos_ = tmp;
        pos_ ^=  PositionToU64(b.file_get(), b.rank_get());
        pos_ |=  PositionToU64(e.file_get(), e.rank_get());
        if (b.file_get() == File::A)
          mov_q = true;
        if (b.file_get() == File::H)
          mov_k = true;
        boo = true;
      }
      else
        pos_ = tmp;
      return boo;
    }
}
