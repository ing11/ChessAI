#include "ray.hh"

namespace plugin
{
  uint64_t Eray(uint64_t p, uint64_t obs)
  {
    uint64_t res = 0;
    uint64_t notAFile = 0xfefefefefefefefe;
    while ((p = p << 1) & notAFile)
    {
      res |= p;
      if (p & obs)
        break;
    }
    return res;
  }

  uint64_t Wray(uint64_t p, uint64_t obs)
  {
    uint64_t res = 0;
    uint64_t notHFile = 0x7f7f7f7f7f7f7f7f;
    while ((p = p >> 1) & notHFile)
    {
      res |= p;
      if (p & obs)
        break;
    }
    return res;
  }

  uint64_t NEray(uint64_t p, uint64_t obs)
  {
    uint64_t res = 0;
    uint64_t notAFile = 0xfefefefefefefefe;
    while ((p = p << 9) & notAFile)
    {
      res |= p;
      if (p & obs)
        break;
    }
    return res;
  }

  uint64_t NWray(uint64_t p, uint64_t obs)
  {
    uint64_t res = 0;
    uint64_t notHFile = 0x7f7f7f7f7f7f7f7f;
    while ((p = p << 7) & notHFile)
    {
      res |= p;
      if (p & obs)
        break;
    }
    return res;
  }
  uint64_t SEray(uint64_t p, uint64_t obs)
  {
    uint64_t res = 0;
    uint64_t notAFile = 0xfefefefefefefefe;
    while ((p = p >> 7) & notAFile)
    {
      res |= p;
      if (p & obs)
        break;
    }
    return res;
  }

  uint64_t SWray(uint64_t p, uint64_t obs)
  {
    uint64_t res = 0;
    uint64_t notHFile = 0x7f7f7f7f7f7f7f7f;
    while ((p = p >> 9) & notHFile)
    {
      res |= p;
      if (p & obs)
        break;
    }
    return res;
  }

  uint64_t Nray(uint64_t p, uint64_t obs)
  {
    uint64_t res = 0;
    while ((p = p << 8))
    {
      res |= p;
      if (p & obs)
        break;
    }
    return res;
  }

  uint64_t Sray(uint64_t p, uint64_t obs)
  {
    uint64_t res = 0;
    while ((p = p >> 8))
    {
      res |= p;
      if (p & obs)
        break;
    }
    return res;
  }
}
