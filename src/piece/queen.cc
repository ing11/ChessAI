#include "queen.hh"

namespace plugin
{

    PieceType Queen::get_type()
    {
      return PieceType::QUEEN;
    }

    uint64_t Queen::get_mvbitb(uint64_t empty)
    {
      uint64_t res = 0;
      uint64_t pos1 = get_pos();
      while (pos1)
      {
        uint64_t pos = pos1 & -pos1;
        pos1 ^= pos;
        res |= Eray(pos, ~empty);
        res |= Wray(pos, ~empty);
        res |= NEray(pos, ~empty);
        res |= NWray(pos, ~empty);
        res |= SEray(pos, ~empty);
        res |= SWray(pos, ~empty);
        res |= Nray(pos, ~empty);
        res |= Sray(pos, ~empty);
      }
      return res & empty;
    }


    uint64_t Queen::get_capbitb(uint64_t en, uint64_t empty)
    {
      uint64_t res = 0;
      uint64_t pos1 = get_pos();
      while (pos1)
      {
        uint64_t pos = pos1 & -pos1;
        pos1 ^= pos;
        res |= Eray(pos, ~empty);
        res |= Wray(pos, ~empty);
        res |= NEray(pos, ~empty);
        res |= NWray(pos, ~empty);
        res |= SEray(pos, ~empty);
        res |= SWray(pos, ~empty);
        res |= Nray(pos, ~empty);
        res |= Sray(pos, ~empty);
      }
      return res & en;
    }


}
