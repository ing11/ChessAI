#include "piece.hh"

namespace plugin
{

 uint64_t PositionToU64 (File file, Rank rank)
 {
   uint64_t one = 1;
   uint64_t ret =  one << ((int)(file) + 8 * (int)(rank));
   return ret;
 }

 Position U64ToPos(uint64_t p)
 {
   int pos = std::log2(p);
   return Position((File)(pos%8), (Rank)(pos/8));
 }


   bool Piece::check_mv(Position p, uint64_t empty)
    {
      return !!(PositionToU64(p.file_get(), p.rank_get())
          & get_mvbitb(empty));
    }

    bool Piece::check_cap(Position p, uint64_t en, uint64_t empty)
    {
      return !!(PositionToU64(p.file_get(), p.rank_get())
          & get_capbitb(en, empty));
    }

    bool Piece::move(Position b, Position e, uint64_t empty, uint64_t en)
    {
      bool boo = false;
      uint64_t tmp = pos_;
      pos_ = PositionToU64(b.file_get(), b.rank_get());

      if (check_mv(e, empty) || check_cap(e, en, empty))
      {
        pos_ = tmp;
        pos_ ^=  PositionToU64(b.file_get(), b.rank_get());
        pos_ |=  PositionToU64(e.file_get(), e.rank_get());
        boo = true;
      }
      else
        pos_ = tmp;
      return boo;
    }

    uint64_t Piece::get_pos()
    {
      return pos_;
    }

    void Piece::set_pos(uint64_t pos)
    {
      pos_ = pos;
    }

    void Piece::cap_atpos(Position p)
    {
      set_pos(PositionToU64(p.file_get(), p.rank_get())
          ^ get_pos());
    }

    bool Piece::check_atpos(Position p)
    {
      return !!(PositionToU64(p.file_get(), p.rank_get())
          & get_pos());
    }

  uint64_t Piece::get_mvsingle(uint64_t single, uint64_t empty)
  {
    uint64_t tmp = pos_;
    pos_ = single;
    uint64_t res = get_mvbitb(empty);
    pos_ = tmp;
    return res;
  }

  uint64_t Piece::get_capsingle(uint64_t single, uint64_t en, uint64_t empty) 
  {
    uint64_t tmp = pos_;
    pos_ = single;
    uint64_t res = get_capbitb(en, empty);
    pos_ = tmp;
    return res;
   }



}
