#pragma once

#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <piece-type.hh>
#include <chessboard.hh>
#include <dlfcn.h>
#include "tool_listener.hh"

namespace parser {
    enum Mask : std::uint8_t {
        ERROR = 0x01,
        CHECK = 0x02,
        CHECKMATE = 0x04,
        TAKEN = 0x08,
        PROMOTED = 0x10
    };

    class Parser {
    public:
        Parser(std::string filename, std::vector<std::string> list);
        ~Parser() = default;

        int parse(bool display);
        bool check_move(char p, std::uint8_t state, plugin::PieceType prom,
                        plugin::Position pos1, plugin::Position pos2, plugin::Color color);
        static plugin::Position* get_pos(char file, char rank);
        static plugin::PieceType get_promo(char prom);

    private:
        std::ifstream ifstr;
        std::vector<std::string> skips;
        std::vector<std::string> commands;
        std::vector<void*> void_list;
        std::vector<plugin::Listener*> listeners;
        plugin::ChessBoard board;
        std::map<std::pair<uint64_t, uint64_t>, uint8_t> saves_;

    // functions to parse
        bool headers();
        bool header();
        bool movetext_section();
        bool element_sequence();
        bool element();
        bool move_number_indication();
        bool game_termination();
        bool lan_move();
        bool an_move(plugin::Color color);
        bool skip();
        bool insert_save();
  };
}
