#include "parser.hh"

namespace parser {
  Parser::Parser(std::string filename, std::vector<std::string> list)
  {
    ifstr.open(filename.c_str());
    skips = std::vector<std::string>();
    commands = std::vector<std::string>();
    void_list = std::vector<void*>();
    for (auto i : list)
    {
      void* lib = dlopen(i.c_str(), RTLD_LAZY);
      void_list.push_back(lib);
      void* symbol = dlsym(lib, "listener_create");
      listeners.push_back(reinterpret_cast<plugin::Listener*(*)()>(symbol)());
    }
  }

  int Parser::parse(bool display = false) {
    if (!ifstr.is_open()) {
      std::cerr << "File opening problem" << std::endl;
      return false;
    }
    plugin::listener_register_board(listeners, board);
    plugin::listener_on_game_started(listeners);
    headers() && movetext_section();
    plugin::listener_on_game_finished(listeners);
    for (auto& i : void_list)
      dlclose(i);
    if (display) {
      std::cout << "Commands: " << std::endl;
      for (auto& elt: commands)
        std::cout << elt << std::endl;
      std::cout << std::endl << "Headers: " << std::endl;
      for (auto& elt: skips)
        std::cout << elt << std::endl;
    }
    return 0;
  }

  bool Parser::headers() {
    int endl = 0;
    while (ifstr.peek() == ' ' || ifstr.peek() == '\n') {
      if (ifstr.get() == '\n')
        endl++;
    }
    if (endl == 2) // blank line equals end of header
      return true;
    return header() && headers();
  }

  bool Parser::header() {
    if (ifstr.get() != '[')
      return false;
    return skip() && ifstr.get() == ']';
  }

  bool Parser::skip() {
    std::string str = "";
    char c;
    int pairs = 1;
    while (!ifstr.eof()) {
      c = ifstr.peek();
      if (c == '[')
        pairs++;
      else if (c == ']')
        pairs--;
      if (pairs == 0)
        break;
      str.push_back(ifstr.get());
    }
    skips.push_back(str);
    return pairs == 0;
  }

  bool Parser::movetext_section() {
    while (ifstr.peek() == ' ' || ifstr.peek() == '\n')
      ifstr.get();
    return element_sequence() && game_termination();
  }

  bool Parser::element_sequence() {
    while ((ifstr.peek() == ' ' || ifstr.peek() == '\n'))
      ifstr.get();
    if (ifstr.eof())
      return true;
    return element() && element_sequence();
  }

  bool Parser::element() {
    return move_number_indication() && lan_move();
  }

  bool Parser::lan_move() {
    while (ifstr.peek() == ' ' || ifstr.peek() == '\n')
      ifstr.get();
    return an_move(plugin::Color::WHITE) && an_move(plugin::Color::BLACK);
  }

  bool Parser::move_number_indication() {
    static int ref = 1;
    int move_nb = -1;
    while (ifstr.peek() == ' ')
      ifstr.get();
    char peek = ifstr.peek();
    if (peek == '*')
      return game_termination();
    else if (peek == '1' || peek == '0') {
      ifstr.get();
      peek = ifstr.peek();
      ifstr.unget();
      if (peek > '9' && peek < '0' && peek != '.') {
        return game_termination();
      }
    }
    ifstr >> move_nb;
    if (ref != move_nb) { // Check order in moves
      //std::cerr << "Order of moves wrong: expected " << ref
      //  << ", got: " << move_nb << std::endl;
      return game_termination();
    }
    ref++;
    if (ifstr.get() != '.' || move_nb == 0)
      return false;
    return true;
  }

  bool Parser::an_move(plugin::Color color) {
    char piece;
    char file;
    char rank;
    char file2;
    char rank2;

    char check;

    const static std::string pieces = "KQRBNP";
    const static std::string promote = "QRBN";

    std::string comm = "";

    piece = ifstr.peek(); //Check name piece

    if (pieces.find(piece) != std::string::npos)
      comm.push_back(ifstr.get());
    else
      piece = 'P';

    if (ifstr.peek() == ' ')
      ifstr.get(); //space

    file = ifstr.get();
    if (file < 'a' || file > 'h') {
      ifstr.unget();
      return game_termination();
    }

    rank = ifstr.get();
    if (rank < '1' || rank > '8')
      return false;
    comm.push_back(file);
    comm.push_back(rank);
    if (promote.find(ifstr.peek()) != std::string::npos) // Search for promotion
      comm.push_back(ifstr.get());

    if (ifstr.peek() != '-' && ifstr.peek() != 'x')
      return false;

    comm.push_back(ifstr.get());

    ifstr >> file2 >> rank2;

    if (file2 < 'a' || file2 > 'h')
      return false;
    if (rank2 < '1' || rank2 > '8')
      return false;
    comm.push_back(file2);
    comm.push_back(rank2);
    char c = ifstr.get(); // get promo or check (first one found)
    if (promote.find(c) != std::string::npos) {// Search for promotion
      comm.push_back(c);
      check = ifstr.get();
      if (check == '+' || check == '#') // search for check or checkmate
        comm.push_back(check);
      else
        ifstr.unget();
      c = ifstr.get();
      if (c != ' ' && c != '\n')
        return false;
    }
    else if (c == '+' || c == '#') { // search for check or checkmate
      comm.push_back(c);
      c = ifstr.get();
      if (c != ' ' && c != '\n')
        return false;
    }

    // TODO: Check command
    std::uint8_t state = 0;
    if (comm.find('+') != std::string::npos)
      state |= parser::CHECK;
    if (comm.find('#') != std::string::npos)
      state |= parser::CHECKMATE;
    if (comm.find('x') != std::string::npos)
      state |= parser::TAKEN;
    if (promote.find(comm.back()) != std::string::npos)
      state |= parser::PROMOTED;

    plugin::PieceType promo = get_promo(comm.back());
    auto pos1 = get_pos(file - 32, rank); // Upcase char
    auto pos2 = get_pos(file2 - 32, rank2); // Upcase char


    //    std::cout << "LE MOVE EST : "  << comm << " ." << std::endl;

    if (check_move(piece, state, promo, *pos1, *pos2, color))
      commands.push_back(comm);
    else
      return false;
    return true;
  }

  bool Parser::check_move(char p, std::uint8_t state, plugin::PieceType prom,
      plugin::Position pos1, plugin::Position pos2, plugin::Color color)
  {
    static double cpt = 1.0;

    auto piece_actual = board[pos1];
    auto taken_piece = board[pos2];

    auto res = board.move(color, pos1, pos2);
    //board.print();

    if (res == plugin::move_ret::FAIL) {
      plugin::listener_on_player_disqualified(listeners, color);
      return false;
    }

    if ((res != plugin::move_ret::CAST_KING && res != plugin::move_ret::CAST_QUEEN)
        && p != (char)piece_actual->first) { // CHECK IF PIECE TYPE IS CORRECT
      plugin::listener_on_player_disqualified(listeners, color);
      return false;
    }

    if ((res == plugin::move_ret::PT &&
          !(state & parser::TAKEN) && !(state & parser::PROMOTED))
        || (res != plugin::move_ret::PT
          && ((state & parser::TAKEN) && (state & parser::PROMOTED)))) {
      plugin::listener_on_player_disqualified(listeners, color);
      return false;
    }

    if (res != plugin::move_ret::PASS && res != plugin::move_ret::PT
        && (((state & parser::TAKEN) && res != plugin::move_ret::TAKEN)
          || (!(state & parser::TAKEN) && res == plugin::move_ret::TAKEN))) {
      plugin::listener_on_player_disqualified(listeners, color);
      return false;
    }

    bool is_check_mat = board.check_checkmate((plugin::Color)(!(bool)color));

    if (((state & parser::CHECKMATE) && !is_check_mat)
        || (!(state & parser::CHECKMATE) && is_check_mat)) {
      plugin::listener_on_player_disqualified(listeners, color);
      return false;
    }

    bool is_check = board.check_check((plugin::Color)(!(bool)color));

    if (!is_check_mat) {
      if (((state & parser::CHECK) && !is_check)
          || (!(state & parser::CHECK) && is_check)) {
        plugin::listener_on_player_disqualified(listeners, color);
        return false;
      }
    }

    if (((state & parser::PROMOTED) && res != plugin::move_ret::PROMOTE
          && res != plugin::move_ret::PT)
        || (!(state & parser::PROMOTED) && res == plugin::move_ret::PROMOTE
          && res == plugin::move_ret::PT)) {
      plugin::listener_on_player_disqualified(listeners, color);
      return false;
    }

    if (piece_actual->first == plugin::PieceType::PAWN) {
      cpt = 1.0;
      saves_.clear();
    }
    plugin::listener_on_piece_moved(listeners, piece_actual->first, pos1, pos2);

    bool cast = res == plugin::move_ret::CAST_QUEEN || res == plugin::move_ret::CAST_KING;
    if (cast) {
      if (res == plugin::move_ret::CAST_KING) {
        plugin::listener_on_kingside_castling(listeners, color);
      }

      if (res == plugin::move_ret::CAST_QUEEN) {
        plugin::listener_on_queenside_castling(listeners, color);
      }
    }
    else {
      if (res == plugin::move_ret::PASS) {
        cpt = 1.0;
        saves_.clear();
        if ((bool)color) {// BLACK
          plugin::listener_on_piece_taken(listeners,
              plugin::PieceType::PAWN,
              plugin::Position(pos2.file_get(), (plugin::Rank)((int)pos2.rank_get() + 1)));
        }
        else {
          plugin::listener_on_piece_taken(listeners, plugin::PieceType::PAWN,
              plugin::Position(pos2.file_get(),
                (plugin::Rank)((int)pos2.rank_get() - 1)));
        }
      }
      else if (state & parser::TAKEN) {
        plugin::listener_on_piece_taken(listeners, taken_piece->first, pos2);
        cpt = 1.0;
        saves_.clear();
      }


      if (state & parser::PROMOTED) {
        board.promote(pos2, prom, color);
        is_check_mat = board.check_checkmate((plugin::Color)(!(bool)color));
        is_check = board.check_check((plugin::Color)(!(bool)color));
        plugin::listener_on_piece_promoted(listeners, prom, pos2);
      }
    }
    cpt+= 0.5;

    if (is_check_mat) {
      plugin::listener_on_player_mat(listeners, (plugin::Color)(!(bool)color));
    }
    else if (is_check) {
      plugin::listener_on_player_check(listeners, (plugin::Color)(!(bool)color));
    }
    else if (board.check_draw((plugin::Color)(!(bool)color))) { // DRAW = PAT (FABIEN)
      plugin::listener_on_player_pat(listeners, (plugin::Color)(!(bool)color));
      plugin::listener_on_draw(listeners);
    }
    else if (cpt >= 50.0) { // REAL DRAW : BONUS
      plugin::listener_on_draw(listeners);
      return false;
    }
    if (insert_save()) {
      plugin::listener_on_draw(listeners);
      return false;
    }

    return true;
  }

  bool Parser::game_termination() {
    std::string str = "";
    while (ifstr.good()) {
      if (ifstr.peek() == '\n')
        break;
      str.push_back(ifstr.get());
    }
    if (str == "1-0") {
      return true;
    }
    if (str == "0-1") {
      return true;
    }
    if (str == "1/2-1/2") {
      return true;
    }
    if (str == "*") {
      return true;
    }
    if (ifstr.eof()) {
      return true;
    }
    return false;
  }

  plugin::PieceType Parser::get_promo(char prom) {
    if (prom == 'K')
      return plugin::PieceType::KING;
    if (prom == 'B')
      return plugin::PieceType::BISHOP;
    if (prom == 'Q')
      return plugin::PieceType::QUEEN;
    if (prom == 'R')
      return plugin::PieceType::ROOK;
    if (prom == 'N')
      return plugin::PieceType::KNIGHT;
    return plugin::PieceType::PAWN;
  }

  plugin::Position* Parser::get_pos(char file, char rank) {
    plugin::File f = plugin::File::A;

    if (file - 'A' == (int)plugin::File::A)
      f = plugin::File::A;
    else if (file - 'A' == (int)plugin::File::B)
      f = plugin::File::B;
    else if (file - 'A' == (int)plugin::File::C)
      f = plugin::File::C;
    else if (file - 'A' == (int)plugin::File::D)
      f = plugin::File::D;
    else if (file - 'A' == (int)plugin::File::E)
      f = plugin::File::E;
    else if (file - 'A' == (int)plugin::File::F)
      f = plugin::File::F;
    else if (file - 'A' == (int)plugin::File::G)
      f = plugin::File::G;
    else if (file - 'A' == (int)plugin::File::H)
      f = plugin::File::H;

    plugin::Rank r = plugin::Rank::ONE;
    if (rank - '1' == (int)plugin::Rank::ONE)
      r = plugin::Rank::ONE;
    else if (rank - '1' == (int)plugin::Rank::TWO)
      r = plugin::Rank::TWO;
    else if (rank - '1' == (int)plugin::Rank::THREE)
      r = plugin::Rank::THREE;
    else if (rank - '1' == (int)plugin::Rank::FOUR)
      r = plugin::Rank::FOUR;
    else if (rank - '1' == (int)plugin::Rank::FIVE)
      r = plugin::Rank::FIVE;
    else if (rank - '1' == (int)plugin::Rank::SIX)
      r = plugin::Rank::SIX;
    else if (rank - '1' == (int)plugin::Rank::SEVEN)
      r = plugin::Rank::SEVEN;
    else if (rank - '1' == (int)plugin::Rank::EIGHT)
      r = plugin::Rank::EIGHT;

    return new plugin::Position(f, r);
  }

  bool Parser::insert_save() {
    auto pair = std::make_pair(board.get_Wpos(), board.get_Bpos());
    auto search = saves_.find(pair);
    if (search != saves_.end()) {
      saves_[pair]++;
      if (saves_[pair] >= 3)
        return true;
    } else {
      saves_.emplace(pair, 1);
    }
    return false;
  }
}

