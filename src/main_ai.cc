#include <iostream>

#include "ai.hh"

int main(int argc, char* argv[])
{
  if (argc != 3)
  {
    std::cerr << "Invalid number of argument" << std::endl;
    std::cerr << "USAGE: ./ai ip port" << std::endl;
    return 1;
  }
  std::string ip(argv[1]);
  std::string port(argv[2]);
  std::string login = "dauver_a";
  AI ai(ip, port, login);
  ai.init();
  ai.play();
}
