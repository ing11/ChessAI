.PHONY: all chessengine human ai clean debug

all: chessengine human ai

chessengine:
	make -C build chessengine
	mv build/chessengine .
human: chessengine
	make -C build human
	mv build/human .
ai: chessengine
	make -C build ai
	mv build/ai .
clean:
	make -C build clean
	rm -f chessengine human ai
debug:
	make clean
	./configure --debug
	make all
check: all
	./tests/test.sh

listen_print_board:
	make -C build listen_print_board
