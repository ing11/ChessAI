#!/bin/sh

green="\033[32m"
red="\033[31m"
white="\033[0m"

cd tests

for file in "openings"/*.pgn; do
  rm -f $file.log
  ./../chessengine --pgn $file -l ./libbasic-output.so > $file.log
  diff $file.log ${file%.pgn}.out
  if [ $? -eq 0 ]; then
    echo -e $green $file $white
  else
    echo -e $red $file $white
  fi
done

for file in "basics"/*.pgn; do
  ./../chessengine --pgn $file -l ./libbasic-output.so > $file.log
  diff $file.log ${file%.pgn}.out
  if [ $? -eq 0 ]; then
    echo -e $green $file $white
  else
    echo -e $red $file $white
  fi
done
